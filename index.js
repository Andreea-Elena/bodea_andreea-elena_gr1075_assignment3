
const FIRST_NAME = "Andreea-Elena";
const LAST_NAME = "Bodea";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name, surname, salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    getDetails(){
       return `${this.name} ${this.surname} ${this.salary }`;
    }
    
}

class SoftwareEngineer extends Employee{
    constructor(name, surname, salary, experience) {
        super(name, surname, salary);
        if(experience==undefined)
        this.experience='JUNIOR';
        else
        this.experience = experience;
    }

    applyBonus(){
        if(this.experience==='JUNIOR')
        return this.salary+this.salary*0.1;
        if(this.experience==='MIDDLE')
        return this.salary+this.salary*0.15;
        if(this.experience==='SENIOR')
        return this.salary+this.salary*0.2;
        return this.salary+this.salary*0.1

    }
   
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

